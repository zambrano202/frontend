import 'package:flutter/material.dart';
import 'package:swiper/src/pages/categorys_page.dart';
import 'package:swiper/src/pages/login_page.dart';
import 'package:swiper/src/pages/register_page.dart';
import 'package:swiper/src/pages/swiper_targe.dart';
// import 'package:swiper/src/pages/resgisterproduct_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: 'Marketplace',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (BuildContext contex) => HomePage(),
        'register': (BuildContext contex) => RegisterPage(),
        'login': (BuildContext context) => LoginPage(),

      },
    );
  }
  
}
