import 'dart:convert';
import 'package:http/http.dart';
import 'package:swiper/src/utils/HttpClient.dart';

class Conection {
  
  HttpClient httpClient;
  String uri;
  Conection(){
    uri = 'http://10.0.2.2:3001/api/marketplace-gateway-ms/products/images';
    httpClient = HttpClient();
  }
  Future<Map> getProduct( int id) async{
    List user;
    try {
      Response response = await httpClient.get('$uri/$id');
      user= jsonDecode(response.body);
      return user[0];
    } catch (err) {
      return null;
    }
  }
}