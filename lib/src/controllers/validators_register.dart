/*
validator of the email field, expect an 
email type text and cannot be empty
error: - email is required
- invalid email
max length 50 characteres
*/
String validateEmail(String value) 
{
  String pattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) 
    {
      return "Email is required";
    } 
      else if (!regExp.hasMatch(value)) 
      {
        return "Invalid email";
      }
        else 
        {
          return null;
        }
 }
/*
password field validator, expect a type of 
text and cannot be empty
error: - password is required
max length 40 characters
*/
String validatePassword(String value) 
{
  String pattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) 
    {
      return "Password is required";
    }
      else 
      {
        return null;
      }
}
/*
validator of the email field, expect an 
email type text and cannot be empty
error: - email is required
- invalid email
max length 50 characteres
 */
String validateEmailAlternate(String value) 
{
  String pattern = 
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0)
   {
    return "Email alternate is required";
   } 
    else if (!regExp.hasMatch(value)) 
      {
        return "Invalid email alternate";
      } 
        else 
        {
          return null;
        }
 }
/*
validator of the email field, expect an 
type text and cannot be empty
error: - name is required
- invalid name
max length 40 letters
*/
 String validateName(String value) 
 {
  String pattern = r'(^[a-z, A-Z]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0)
    {
     return "Name is required";
    } 
    else if (!regExp.hasMatch(value)) 
      {
        return "type only letters";
      }
  return null;
 }
/*
validator of the lastName field, expect an 
type text and cannot be empty
error: - last name is required
- invalid last name
max length 40 letters
*/
String validateLastName(String value) 
{
  String pattern = r'(^[a-z, A-Z]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) 
    {
     return "Last name is required";
    } 
      else if (!regExp.hasMatch(value)) 
      {
         return "type only letters";
      }
  return null;
 }
/*
validator of the documentNumber field, expect an 
email type number and cannot be empty
error: - document number is required
- The number must have 10 digits
- type only numbers
max length 10 numbers
*/
 String validateDocumentNumber(String value)
  {
  String patttern = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0)
   {
     return "Document number is required";
   }
    else if (value.length != 10)
      {
        return "The number must have 10 digits";
      }
        else if (!regExp.hasMatch(value))
          {
            return "type only numbers";
          }
  return null;
 }
/*
validator of the phone field, expect an 
email type number and cannot be empty
error: - phone is required
- The number must have 10 digits
- type only numbers
max length 10 numbers
*/
String validateMobile(String value) {
  String patttern = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0)
   {
     return "Phone is required";
   }
    else if (value.length != 10) 
      {
        return "The number must have 10 digits";
      }
        else if (!regExp.hasMatch(value))
          {
          return "type only numbers";
          }
  return null;
 }
/*
validator of the address field, expect an 
type text and cannot be empty
error: - address is required
- invalid address
max length 40 letters
*/
String validateAddress(String value) {
  String pattern = r'(^[a-z, A-Z]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0)
    {
     return "Address is required";
    } 
      else if (!regExp.hasMatch(value)) 
      {
        return "type only letters";
      }
   return null;
 }
 