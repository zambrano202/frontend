import 'package:flutter/cupertino.dart';
import 'package:swiper/src/providers/loginProvider.dart';
import 'package:swiper/src/services/alert_login.dart';
import 'package:swiper/src/utils/token.dart';


class LoginController {
  LoginProvidier loginProvidier;
    Alerts alert;
    Token token;

  LoginController() {
    loginProvidier = LoginProvidier();
    alert = Alerts();
    token = new Token();
  }

  void login(BuildContext context, data) {
    print(data['email']);
    print(data['password']);
   


    loginProvidier.login(data).then((res) {
      print(data);
      if( res == null ){
        alert.errorAlert(context, 'Failed authentication');
      }
      else {
        alert.successLoginAlert(context, 'Welcome ' + data['email']);
        print(res);
        token.setToken(res['token']);

      }
    }).catchError((err) {
      print(err);
      alert.errorAlert(context, 'authentication error');
    });
  }
}