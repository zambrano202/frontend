import 'dart:async'; 
import 'dart:convert';
import 'dart:io';
import 'package:mime_type/mime_type.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:swiper/src/utils/HttpClient.dart';

class ProductProvider {

Future<String> uploadimage( File image) async {

  final url = Uri.parse('https://api.cloudinary.com/v1_1/dmqw8t6hl/image/upload?upload_preset=zvwgoa3m');
  final mimeType = mime(image.path).split('/'); //image/jpg

  final imageUpload = http.MultipartRequest(
    'POST',
    url
  );

  final file = await http.MultipartFile.fromPath(
    'file',
    image.path,
    contentType: MediaType( mimeType[0], mimeType[1] )
  );

  //multiple achievers can be attached as well
  imageUpload.files.add(file);

  final streamResponse = await imageUpload.send();
  final resp = await http.Response.fromStream(streamResponse);

  if (resp.statusCode != 200 && resp.statusCode !=201) { 
    print('está mal');
    print(resp.body);
    return null;
  }

  final respData = json.decode(resp.body);
  print(respData);

  return respData['secure_url'];



} 

}