

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Alert {

 void errorAlert(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            title: Text('Advertencia!'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(message),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: FadeInImage(
                    height: 90.1,
                    width: 90.0, 
                    image: AssetImage("assets/signo.gif"), placeholder:AssetImage("assets/signo.gif")
                  ),
                  
                ),  
                Container(
                  child: RaisedButton(
                    child: Text('Regresar'),
                    color: Colors.blue,
                    textColor: Colors.white,
                    shape: StadiumBorder(),
                    onPressed: (){
                      Navigator.pushNamed(context, '/');
                    },
                  )
                  ),
              
              ],
              
            ),
          );
        });
 }
   
}