import 'dart:convert';

import 'package:http/http.dart';
import 'package:swiper/src/utils/HttpClient.dart';
import 'package:swiper/src/utils/token.dart';

class LoginProvidier{
    HttpClient http;
  LoginProvidier(){
    http = HttpClient();
  }
  
  Future<Map> login(data)async{
    Map users;
    try {
  Response res = await http.post('http://10.0.2.2:3001/api/marketplace-gateway-ms/users/login', data);
      users =jsonDecode(res.body);
      
      print(users['token']);
      Token().setToken(users['token']);
    } catch (err) {
      print(err);
    }  
    return users;
  }
}