import 'dart:convert';
import 'package:http/http.dart';
import 'package:swiper/src/config/UrlConfig.dart';
import 'package:swiper/src/utils/HttpClient.dart';


class ProductsProvider {
  HttpClient http;
  String url = UrlConfig().getUri();
  String uri;

  ProductsProvider(){
    http = HttpClient();
    uri = "$url/products";
  }

  Future<List> getproduct(id) async {
    List products;
    try {
    Response res = await http.get('$uri/categorys/$id');
      products = json.decode(res.body);

      return products;
    }catch(error) {
        return null;
    }
  }
}