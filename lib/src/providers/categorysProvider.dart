import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:swiper/src/config/UrlConfig.dart';
import 'package:swiper/src/utils/HttpClient.dart';

class CategoriesProvider{
  HttpClient client;
  List<dynamic> categories = [];
 
  String url = UrlConfig().getUri();
  Future <List<dynamic>> categoriesList(id) async{
    http.Response res = await http.get('$url/productscategory/$id');
     categories =  json.decode(res.body); 
   
     
     return categories;
   }
}