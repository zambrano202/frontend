import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:swiper/src/pages/swiper_targe.dart';
import 'package:swiper/src/utils/HttpClient.dart';
import 'package:swiper/src/utils/ProductProvider.dart';
import 'package:http/http.dart' as http;

class Search extends SearchDelegate{

  final productsProvider = new ProductProvider();
  List data ;
  List productsdata;

  @override
  List<Widget> buildActions(BuildContext context) {
    // acciones de nuestro Appbar-como iconos de limpiar o cancelar busqueda
    return [
      IconButton(icon: Icon(Icons.clear),
      onPressed: () {
        query = '';
      },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // icono a la izquierda del AppBar
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // crea los resultados que vamos a mostrar
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // sugerencias que aparecen cuando la persona escribe
   
    getProducts (query) async {
      HttpClient client = new HttpClient();
      http.Response  response = await client.get('http://10.0.2.2:3001/api/marketplace-gateway-ms/products/name/$query');
      print(response);
      data = json.decode(response.body);
      productsdata = data;

      return productsdata;

    }

   if( query.isEmpty){
     return Container();
   }

   return FutureBuilder(
     future: getProducts(query),
     builder: (BuildContext context, AsyncSnapshot snapshot){
       if(snapshot.hasData){
        return ListView.builder(
          itemCount: productsdata == null ? 0 : productsdata.length,
          itemBuilder: (context, index) => gets(context,  index),

        );

       } else{
         return Center(
           child: CircularProgressIndicator(),
          );
       }
     },
   );
  }

   Widget gets(BuildContext context, index) {
    return ListTile(
      leading: FadeInImage(
      image: NetworkImage(productsdata[index]["url1"]),
      placeholder: AssetImage('assets/no-image.png'),
      width: 50.0,
      fit: BoxFit.contain,
      ),
      title: Text(productsdata[index]["nameProduct"]),
      onTap: (){
        close(context, null);

        final id = productsdata[index]["idproduct"];
                print(id);
                final route = MaterialPageRoute(
                  builder: (context){
                    return SwiperTarge(id: id,);
                  }
                );
                Navigator.push(context, route);
      },
    );
  }
}