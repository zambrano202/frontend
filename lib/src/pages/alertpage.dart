import 'package:flutter/material.dart';
import 'package:swiper/src/providers/categorysProvider.dart';
import 'package:swiper/src/utils/alerts.dart';

class Ejemplo extends StatefulWidget {
  final int idCat;
  Ejemplo({this.idCat});

  @override
  EjemploState createState() => EjemploState(idCategories: this.idCat);
}


class EjemploState extends State<Ejemplo> {

final int idCategories;
Alert alert;
CategoriesProvider categoriesProvider;
List<dynamic> categoriesData = new List();

EjemploState({this.idCategories}){
  this.alert = Alert();
  this.categoriesProvider = CategoriesProvider();
getCategories();
}

Future getCategories() async {
categoriesData = await categoriesProvider.categoriesList(idCategories);

print(categoriesData);

  if(categoriesData.length == 0){
    alert.errorAlert(context, "Lo Siento En Este Momento No Hay Productos Disponibles Para Esta Categoria");
  }
  setState(() {  });
}



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Categoria'),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          )
        ],

      ),
      body: ListView(
        padding: EdgeInsets.all(20.0),

        children: <Widget>[
        ],
      )

    );
  }
}