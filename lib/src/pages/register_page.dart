import 'package:flutter/material.dart';
import 'package:swiper/src/controllers/register_controller.dart';
import 'package:swiper/src/controllers/validators_register.dart';
void main() => runApp(RegisterPage());

class RegisterPage extends StatefulWidget {

  final String title;
  RegisterPage({this.title});

 @override
 _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
 GlobalKey<FormState> keyForm = new GlobalKey();
 TextEditingController  emailCtrl = new TextEditingController();
 TextEditingController  passwordCtrl = new TextEditingController();
  TextEditingController  emailAlternateCtrl = new TextEditingController();
 TextEditingController  nameCtrl = new TextEditingController();
  TextEditingController  lastNameCtrl = new TextEditingController();
 TextEditingController  documentNumberCtrl = new TextEditingController();
 TextEditingController  mobileCtrl = new TextEditingController();
  TextEditingController  addressCtrl = new TextEditingController();
   SigninController signinController;

  _RegisterPageState(){
    this.emailCtrl = TextEditingController();
    this.passwordCtrl = TextEditingController();
    this.emailAlternateCtrl = TextEditingController();
    this.lastNameCtrl = TextEditingController();
    this.lastNameCtrl = TextEditingController();
    this.documentNumberCtrl = TextEditingController();
    this.mobileCtrl = TextEditingController();
    this.addressCtrl = TextEditingController();
    this.signinController = SigninController();

  }

 @override
 Widget build(BuildContext context) {
   return MaterialApp(
     debugShowCheckedModeBanner: false,
     home: new Scaffold(
       
       appBar: new AppBar(
          title: Text('Register User', 
          style: TextStyle(color: Colors.black)
          ),
          leading: IconButton(
      icon: Icon(
        Icons.home, 
        color: Colors.black,

      ),
      onPressed: () {
        Navigator.of(context).pushNamed('login');   
       
        },
    ),


  actions: <Widget>[
    IconButton(
      alignment: Alignment.center,
      icon: Icon(
        Icons.account_circle,
        color: Colors.black,

      ),
      onPressed: () {
        Navigator.of(context).pushNamed('login');   
       
        },
    )
  ],
  
       ),
       body: new SingleChildScrollView(
         child: new Container(
           margin: new EdgeInsets.all(10.0),
           child: new Form(
             key: keyForm,
             child: formUI(),
           ),
         ),
       ),
     ),
   );
 }



 formItemsDesign(icon, item) {
   return Padding(
     padding: EdgeInsets.symmetric(vertical: 1),
     child: Card(child: ListTile(leading: Icon(icon), title: item)),
   );
   
 }


 Widget formUI() {
   
   return  Column(
     children: <Widget>[

      // field email 
      formItemsDesign(
           Icons.email,
           TextFormField(
             controller: emailCtrl,
               decoration: new InputDecoration(
                 labelText: 'Email',
               ),
               keyboardType: TextInputType.emailAddress,
               maxLength: 50,
               validator: validateEmail,)),
      
      // fiel password
      formItemsDesign(
           Icons.lock,
           TextFormField(
             controller: passwordCtrl,
             obscureText: true,
             decoration: InputDecoration(
               labelText: 'Password',
             ),
             maxLength: 40,
             validator: validatePassword,
           )), 

           formItemsDesign(
           Icons.email,
           TextFormField(
             controller: emailAlternateCtrl,
               decoration: new InputDecoration(
                 labelText: 'Email Alternate',
               ),
               keyboardType: TextInputType.emailAddress,
               maxLength: 50,
               validator: validateEmailAlternate,)),

       formItemsDesign(
           Icons.account_circle,
           TextFormField(
             controller: nameCtrl,
             decoration: new InputDecoration(
               labelText: 'Name',
             ),
              keyboardType: TextInputType.text,
              maxLength: 40,
             validator: validateName,
           )),

           formItemsDesign(
           Icons.account_box,
           TextFormField(
             controller: lastNameCtrl,
             decoration: new InputDecoration(
               labelText: 'Last Name',
             ),
              keyboardType: TextInputType.text,
              maxLength: 40,
             validator: validateLastName,
           )),

           formItemsDesign(
           Icons.format_list_numbered,
           TextFormField(
             controller: documentNumberCtrl,
               decoration: new InputDecoration(
                 labelText: 'Document Number',
               ),
               keyboardType: TextInputType.number,
               maxLength: 10,
               validator: validateDocumentNumber,)),

       formItemsDesign(
           Icons.phone,
           TextFormField(
             controller: mobileCtrl,
               decoration: new InputDecoration(
                 labelText: 'Cellphone',
               ),
               keyboardType: TextInputType.number,
               maxLength: 10,
               validator: validateMobile,)),

        formItemsDesign(
           Icons.home,
           TextFormField(
             controller: addressCtrl,
             decoration: new InputDecoration(
               labelText: 'Adress',
             ),
              keyboardType: TextInputType.text,
              maxLength: 40,
             validator: validateAddress,
           )),
      
      
    
   GestureDetector(
   onTap: (){
      if(keyForm.currentState.validate()){

     signinController.signin(context, {'email': this.emailCtrl.text, 'password':this.passwordCtrl.text, 
     'emailAlternate':this.emailAlternateCtrl.text, 'name':this.nameCtrl.text, 'lastName':this.lastNameCtrl.text,
     'numDocument':this.documentNumberCtrl.text,'cellPhone':this.mobileCtrl.text, 'address':this.addressCtrl.text,});
      

       }

       else{

       }
   }
   ,child: Container(
     
         margin: new EdgeInsets.all(0.0),
         alignment: Alignment.center,
         decoration: ShapeDecoration(
           shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.circular(10.0)),
           gradient: LinearGradient(colors: [
             Color(0xFF0EDED2),
             Color(0xFF03A0FE),
           ],
               begin: Alignment.topLeft, end: Alignment.bottomRight),
         ),
         child: Text("Register",
             style: TextStyle(
                 color: Colors.black,
                 fontSize: 20,
                 fontWeight: FontWeight.w500)),
         padding: EdgeInsets.only(top: 14, bottom: 14),
       ))
     ],
   );
 }
}
