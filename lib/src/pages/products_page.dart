import 'package:flutter/material.dart';
import 'package:swiper/src/pages/swiper_targe.dart';
import 'package:swiper/src/providers/productsProvider.dart';



class ProductsPage extends StatefulWidget {
 
 final int id;

 ProductsPage({this.id, idCat});

  @override
  _ProductsPageState createState() => _ProductsPageState(id);
}

class _ProductsPageState extends State<ProductsPage> {
  final ProductsProvider products = ProductsProvider();
  final int idproduct;
  List data;
  List listproduct;
  


  _ProductsPageState(this.idproduct){
    products.getproduct(idproduct).then((res){
      setState((){
        listproduct = res;
        print(listproduct);
     
      });
    
  });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Productos'),
      ),
      body: ListView.builder(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(17.0),
        itemCount: listproduct == null ? 0 : listproduct.length,
        itemBuilder: (BuildContext context, int index) => card(context, index ),
      ),
    );
  }

  Widget myDetailsContainer1(BuildContext context,   int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(child: Text('${listproduct[index]["nameProduct"]}',
            style: TextStyle(color: Colors.blue, fontSize: 24.0,fontWeight: FontWeight.bold),)),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(child: Text('Precio:' + '${listproduct[index]["cost"]}'.toString(),
                    style: TextStyle(color: Colors.black, fontSize: 18.0,),)),
                ],)),
        ),
      ],
    );
  }

 Widget card(BuildContext context , index) {
   return Container(

     padding: EdgeInsets.all(10.0),
     child: ListTile(
       onTap: (){
         final   id = listproduct[index]["idproduct"];
         final route = MaterialPageRoute(
           builder: (context){
             return SwiperTarge(id: id);
           }
         );
         Navigator.push(context, route);
       },
       title: new FittedBox(
       child: Material(
           color: Colors.white,
           elevation: 14.0,
           borderRadius: BorderRadius.circular(24.0),
           shadowColor: Color(0x802196F3),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[ 
                 Container(
                 child: Padding(
                   padding: const EdgeInsets.only(left: 16.0),
                   child: myDetailsContainer1(context, index),
                 ),
               ),
                
               Container(
                 width: 350,
                 height: 200,
                 child: ClipRRect(
                   borderRadius: new BorderRadius.circular(24.0),
                  child: Image(
                     fit: BoxFit.contain,
                     alignment: Alignment.topRight,
                     image: NetworkImage(listproduct[index]["Url1"].toString()),
                   ),
                 ),),
             ],)
       ),
     ),
     )
     
   );
  }
}
