import 'package:flutter/material.dart';
import 'package:swiper/src/controllers/loginController.dart';
import 'package:swiper/src/providers/loginProvider.dart';

class LoginPage extends StatelessWidget {

String title;
TextEditingController emails = new TextEditingController();
TextEditingController passwords = new TextEditingController();
LoginController loginController;
LoginProvidier loginProvidier;


LoginPage({this.title}){

  this.emails = TextEditingController();
  this.passwords = TextEditingController();
  this.loginController = LoginController();
  this.loginProvidier = LoginProvidier();

}

  @override
  Widget build(BuildContext context) {

    final email = TextFormField(
      controller: emails,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      hintText: "Email",
      border:
      OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

  

    final password = TextFormField(
      controller: passwords,
      
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          loginController.login(context, {'email': emails.text, 'password': passwords.text});
          
        },
        child: Text("Login",
            textAlign: TextAlign.center,
      ),
      )
      );


    final backtext = FlatButton(
      child: Text(
        'Forget password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
      },
    );


    return Scaffold(
      appBar: new AppBar(
          title: Text('Login', 
          style: TextStyle(color: Colors.black)
          ),
          leading: IconButton(
      icon: Icon(
        Icons.home, 
        color: Colors.black,
      ),
      onPressed: () {
        Navigator.of(context).pushNamed('/');   
       
        },
    ),
    actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Registrarse"),
              onPressed: () {
                Navigator.of(context).pushNamed('register');
              },
            ),
          ],
      ),

      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          children: <Widget>[
            // Text('happy birthday login),
          SizedBox(height: 30.0),
            SizedBox(
              height: 150.0,
              child: Image.asset(
               "assets/logo.png",
               fit: BoxFit.contain,
                  ),
                ),
            SizedBox(height: 30.0),
            email,
            SizedBox(height: 30.0),
            password,
            SizedBox(height: 24.0),
            loginButton,
  
            backtext
          ],
        ),
      ),
    );
  }
}





