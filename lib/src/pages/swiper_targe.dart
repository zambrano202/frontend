import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:swiper/src/providers/SwiperProvider.dart';
import 'package:swiper/src/search/search_delegate.dart'; 

class SwiperTarge extends StatefulWidget {
  final int id;
  SwiperTarge({this.id});

  @override
  _SwiperTargeState createState() => _SwiperTargeState(id);
}

class _SwiperTargeState extends State<SwiperTarge> {
  SwiperProvider swiper = SwiperProvider();
  final int id;
  List  productdetails;

  _SwiperTargeState(this.id) {
    print(id);
    swiper.getproductdetail(id).then((res){
      setState(() {
        productdetails = res;
        print(productdetails);
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 30),
              child: Text('Productos'),
            )
          ],
        ),
        //button search
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: Search(),
              );
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _swiperTarge(),
              _title(),
              _value(),
              _text(),
              _description(),
                Container(
                  padding: EdgeInsets.only(left: 120, right: 120, bottom: 40),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: _button(),
                      ),
                    ],
                  ),
                ),
            ],
        ),
      ),
    ));
  }

  _swiperTarge() {
    return Center(
      child: new Container(
        padding: EdgeInsets.all(20.0),
        height: 200.0,
              child: Carousel (
          boxFit: BoxFit.cover,
          images: [
            NetworkImage('${productdetails[0]['Url1']}'),
            NetworkImage('${productdetails[0]['Url2']}'),
            NetworkImage('${productdetails[0]['Url3']}')
          ],
          animationCurve: Curves.fastOutSlowIn,
          animationDuration: Duration(milliseconds: 2000),
          
        ),
      ),
    ) ;
  }

  _title() {
    return Container(
      padding: EdgeInsets.only(top: 30, bottom: 15),
      child: Center(
        child: Text(
          '${productdetails[0]['nameProduct']}',
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  _value() {
    return Container(
      padding: EdgeInsets.only(right: 130),
      child: Center(
        child: Text(
          'costo: ' + '${productdetails[0]['cost']}',
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  _text() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Center(
          child: Container(
        padding: EdgeInsets.only(left: 50, right: 50, bottom: 10),
        child: Text(
          '${productdetails[0]['characteristics']}',
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
      )),
    );
  }

  _description() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Center(
          child: Container(
        padding: EdgeInsets.all(10.0),
        child: Text(
          '',
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
      )),
    );
  }

  _icon() {
    return Icon(
      Icons.shopping_cart,
    );
  }

  _button() {
    return MaterialButton(
      minWidth: 50.0,
      height: 30.0,
      onPressed: () {

      },
      color: Colors.lightBlue,
      child: Text('Comprar', style: TextStyle(color: Colors.white)),
    );
  }
}
