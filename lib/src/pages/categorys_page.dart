import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:swiper/src/pages/products_page.dart';
import 'package:swiper/src/search/search_delegate.dart';
import 'dart:convert';

import 'package:swiper/src/utils/HttpClient.dart';


class HomePage extends StatefulWidget {

  @override
    _HomePage createState()=> _HomePage();

}

class _HomePage extends State<HomePage>{
  int id;
  List data;
  List categoriesData;


  getCategories() async {

    HttpClient client = new HttpClient();
    http.Response response = await client.get('http://10.0.2.2:3001/api/marketplace-gateway-ms/categories');

    print(response.body);

    data = json.decode(response.body);

    setState(() {
      categoriesData = data;
    });
  }
  getId(int id) {

  }

  @override 
  void initState(){
    super.initState();
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Categoria'),
        
        backgroundColor: Colors.indigoAccent,
        
        actions: <Widget>[
         FlatButton(
           onPressed: (){
             Navigator.of(context).pushNamed('register');
           },
            child: Text('Registrarme'),
          ),
           FlatButton(
           onPressed: (){
             Navigator.of(context).pushNamed('login');
           },
            child: Text('Login'),
          ),

          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              showSearch(
                context: context,
                delegate: Search(),
              );
            },
          ),
       
         
        ],
        
      ),



      body: ListView.builder(
        padding: EdgeInsets.all(20.0),

          itemCount: categoriesData == null ? 0 : categoriesData.length,
          itemBuilder: (BuildContext context, int index) => _cardCategory(context, index),
      )

    );
  }

   Widget _cardCategory( BuildContext context, int index){

     return Card(
       elevation: 10.0,
        child: Column(
          children: <Widget>[
               ListTile(
              leading: Icon(Icons.category, color:Colors.blue),
              title: Text('${categoriesData[index]["nomcategory"]}'),
              onTap: (){

                final id = categoriesData[index]["idcategory"];
                final route = MaterialPageRoute(
                  builder: (context){
                    return ProductsPage(id: id);
                  }
                );
                Navigator.push(context, route);
              },
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                
                ],
            ),
            
          ],
        ),
       
     );

   }

}