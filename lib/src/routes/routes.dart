import 'package:flutter/cupertino.dart';
import 'package:swiper/src/pages/categorys_page.dart';
import 'package:swiper/src/pages/login_page.dart';
import 'package:swiper/src/pages/products_page.dart';
import 'package:swiper/src/pages/register_page.dart';
import 'package:swiper/src/pages/swiper_targe.dart';

Map<String, WidgetBuilder> getroutes() {
  return <String, WidgetBuilder> {
    '/' : (BuildContext context) => HomePage(),
    'register': (BuildContext context) => RegisterPage(),
    'login' : (BuildContext context) => LoginPage(),
    'swiper' : (BuildContext context) => SwiperTarge(),
    'products' : (BuildContext context) => ProductsPage(),
  };
}